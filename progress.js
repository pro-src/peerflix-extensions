#!/usr/bin/env node

const chalk = require('chalk');
const ip = require('ip');
const numeral = require('numeral');
const smoothWriter = require('smooth-writer');

module.exports = progress;

function progress(engine, stream = process.stderr) {
  const write = smoothWriter(stream);

  let hotswaps = 0;
  let verified = /* pieces */ 0;
  let invalid = /* pieces */ 0;
  let downloaded = /* percentage */ 0;

  let { swarm, swarm: { wires } } = engine;

  let started = Date.now();

  engine.on('hotswap', () => { hotswaps++; });
  engine.on('invalid-piece', () => { invalid++; });
  engine.on('verify', () => {
    verified++;
    downloaded = Math.floor(verified / engine.torrent.pieces.length * 100);
  });

  engine.on('verifying', function verifying() {
    const verify = i => {
      const percentage = Math.round(((i + 1) / engine.torrent.pieces.length) * 100.0)

      write(chalk`{yellow Verifying downloaded:} ${percentage}%`);
    };

    verify(-1);

    engine.on('verify', verify);
    engine.on('ready', () => {
      engine.removeListener('verify', verify);
      engine.removeListener('verifying', verifying);
    });
  });


  swarm.on('wire', fetchingMetaData);
  engine.on('ready', () => swarm.removeListener('wire', fetchingMetaData));

  function fetchingMetaData() {
    write(chalk`{green Fetching torrent metadata from} {bold ${engine.swarm.wires.length}} {green peers}`);
  }

  engine.server.on('listening', () => {
    const { server, server: { index } } = engine;
    const href = `http://${ip.address()}:${server.address().port}/`;

    const file = {
      name: index.name.split('/').pop().replace(/\{|\}/g, ''),
      length: index.length
    };

    const whitespace = Array(stream.columns).join(' ');
    const trim = (len, str) => whitespace.slice(0, Math.max(len-str.length, 0)) + str;
    const bytes = i => numeral(i).format('0.0b');

    function draw() {
      const unchoked = engine.swarm.wires.filter(wire => !wire.peerChoking);
      const runtime = Math.floor((Date.now() - started) / 1000);

      let { rows: remaining } = stream;
      let listed = /* peers */ 0;

      write(chalk`{yellow info} {green location} {bold ${href}}`);
      write(chalk`{yellow info} {green streaming} {bold ${file.name} (${bytes(file.length)})}`);
      write(chalk`{yellow info} {green downloading} {bold ${bytes(swarm.downloadSpeed())}/s} {green from} {bold ${unchoked.length}/${wires.length}} {green peers}`);
      write(chalk`{yellow info} {green path} {cyan ${engine.path}}`);
      write(chalk`{yellow info} {green downloaded} {bold ${bytes(swarm.downloaded)}} (${downloaded}%) {green and uploaded} {bold ${bytes(swarm.uploaded)}} {green in} {bold ${runtime}s} {green with} {bold ${hotswaps}} {green hotswaps}     `);
      write(chalk`{yellow info} {green verified} {bold ${verified}} {green pieces and received} {bold ${invalid}} {green invalid pieces}`);
      write(chalk`{yellow info} {green peer queue size is} {bold ${swarm.queued}}`);
      write();

      remaining -= 10;

      wires.every(wire => {
        const tags = [];

        if (wire.peerChoking) {
          tags.push('choked');
        }

        write(chalk`{magenta ${trim(25, wire.peerAddress)}} ${trim(10, bytes(wire.downloaded))} {cyan ${trim(10, bytes(wire.downloadSpeed()))}/s} {grey ${trim(15, tags.join(', '))}}   `);
        return remaining - (++listed) > 4;
      });

      remaining -= listed;

      if (wires.length > listed) {
        write();
        write(`... and ${wires.length - listed} more     `);
      }

      write();
      setTimeout(draw, 500);
    }

    draw();
  });
}
